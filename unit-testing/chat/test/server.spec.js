const server = require('../server.js');
const request = require('request-promise');
const config = require('../config/index.js');
const fs = require('fs');
const assert = require('assert');
const path = require('path');

describe('GET request', () => {
	let app;

	const PORT = 3333;
	const BASE_URI = `http://localhost:${PORT}/`;

	before((done) => {
		app = server.listen(PORT, () => {
			done();
		});
	});

	it('Go to: http://localhost:3333/ => return index.html file', async () => {
		const fileName = 'index.html';
        const filePath = path.join(config.publicRoot, fileName);

		const res = await request(
            {
                uri: BASE_URI,
                method: 'GET'
            }
        );

		const fileContent = fs.readFileSync(filePath, {encoding: 'utf-8'});
        assert.strictEqual(res, fileContent, 'Not index.html');
    });

	it('Go to: http://localhost:3333/norm.txt => return norm.txt file', async () => {
	    const fileName = 'norm.txt';
	    const filePath = path.join(config.filesRoot, fileName);

	    fs.writeFileSync(filePath, 'Hello content!', {encoding: 'utf-8'});

	    const res = await request(
            {
                uri: BASE_URI + fileName,
                method: 'GET'
            }
        );

        const fileContent = fs.readFileSync(filePath, {encoding: 'utf-8'});
        fs.unlinkSync(filePath);
        assert.strictEqual(res, fileContent, 'Not norm.txt');
    });

	// Написать тесты на
	// get запрос к http://localhost:3333  => вернет index.html
	// get запрос к http://localhost:3333/file.txt => вернет file.txt
	after((done) => {
		app.close(() => {
			done();
		});
	});

});
